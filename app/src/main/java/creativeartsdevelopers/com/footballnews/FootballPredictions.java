package creativeartsdevelopers.com.footballnews;

import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.KeyEvent;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.webkit.WebChromeClient;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;

import io.netopen.hotbitmapgg.library.view.RingProgressBar;

public class FootballPredictions extends AppCompatActivity {

    private WebView myWebView;
    RingProgressBar mBar;
    private AdView adView;

    private final String TAG = FootballPredictions.class.getSimpleName();
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_football_predictions);


        adView = new AdView(this, this.getResources().getString(R.string.banner), AdSize.BANNER_HEIGHT_50);
        LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();

        interstitialAd = new InterstitialAd(this, this.getResources().getString(R.string.interstitial));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();


        myWebView = findViewById(R.id.webview);
        mBar = findViewById(R.id.progress);
        mBar.setMax(100);
        WebSettings myWebSettings = myWebView.getSettings();
        myWebSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(mWebViewClient);
        myWebView.setWebChromeClient(mWebChromeClient);
        myWebView.loadUrl("https://oddslot.com/tips/");
    }

    WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public void onPageFinished(WebView view, String url) {
            mBar.setVisibility(View.GONE);
            mBar.setProgress(100);

            myWebView.loadUrl("javascript:(function() { " +
                    "var head = document.getElementsByClassName('header-mobile__inner')[0].style.display='none'; " +
                    "var head = document.getElementsByClassName('page-heading')[0].style.display='none'; " +
                    "var head = document.getElementsByClassName('footer')[0].style.display='none'; " +
                    "var head = document.getElementsByClassName('mobile-banner')[0].style.display='none'; " +
                    "var head = document.getElementsByClassName('header-mobile__logo')[0].style.display='none'; " +
                    "var head = document.getElementsByClassName('qa-disclaimer')[0].style.display='none'; " +
                    "})()");
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            mBar.setVisibility(View.VISIBLE);
            mBar.setProgress(0);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            myWebView.loadUrl("file:///android_asset/error.html");

        }
    };

    WebChromeClient mWebChromeClient = new WebChromeClient() {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            mBar.setProgress(newProgress);
        }

    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
