package creativeartsdevelopers.com.footballnews;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;


public class Dashboard extends AppCompatActivity implements OnClickListener {

    private CardView prediction,scores,tables,news,fixtures,vipPrediction;

    private AdView adView;

    private final String TAG = Dashboard.class.getSimpleName();
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        adView = new AdView(this, this.getResources().getString(R.string.banner), AdSize.BANNER_HEIGHT_50);
        LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();

        interstitialAd = new InterstitialAd(this, this.getResources().getString(R.string.interstitial));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();

        prediction = findViewById(R.id.predictionId);
        scores =  findViewById(R.id.scoresId);
        tables =  findViewById(R.id.tablesId);
        fixtures = findViewById(R.id.fixturesId);
        news =  findViewById(R.id.newsId);
        vipPrediction = findViewById(R.id.vipPrediction);


        prediction.setOnClickListener(this);
        scores.setOnClickListener(this);
        tables.setOnClickListener(this);
        fixtures.setOnClickListener(this);
        news.setOnClickListener(this);

        vipPrediction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comingsoonDialog();
            }
        });

    }

    @Override
    public void onClick(View view) {
        Intent i;

        switch (view.getId()) {
            case R.id.predictionId : i = new Intent(this,FootballPredictions.class);startActivity(i); break;
            case R.id.scoresId : i = new Intent(this,LiveScores.class);startActivity(i); break;
            case R.id.tablesId : i = new Intent(this,LeagueTables.class);startActivity(i); break;
            case R.id.fixturesId : i = new Intent(this,FootballFixtures.class);startActivity(i); break;
            case R.id.newsId : i = new Intent(this,FootballNews.class);startActivity(i); break;
            default:break;
        }

    }
    private void comingsoonDialog() {

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.comingsoon, null);

        Button cancelButton = view.findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent splashActivity = new Intent(getApplicationContext(),Dashboard.class) ;
                startActivity(splashActivity);
            }
        });

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(view)
                .create();
        alertDialog.show();

    }


}
